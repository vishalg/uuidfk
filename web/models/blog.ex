defmodule Uuuidpk.Blog do
  use Uuuidpk.Web, :model

  @primary_key {:id, Ecto.UUID, read_after_writes: true}
  
  schema "blogs" do
    field :desc, :string
    has_many :tags, Uuuidpk.Tag

    timestamps
  end

  before_insert Uuuidpk.UUID, :put_uuid, []

  @required_fields ~w(desc)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
