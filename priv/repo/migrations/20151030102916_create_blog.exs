defmodule Uuuidpk.Repo.Migrations.CreateBlog do
  use Ecto.Migration

  def change do
   	execute "CREATE EXTENSION \"uuid-ossp\""

    create table(:blogs, primary_key: false) do
	  add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :desc, :string

      timestamps
    end

  end
end
