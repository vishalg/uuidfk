defmodule Uuuidpk.Repo.Migrations.CreateTag do
  use Ecto.Migration

  def change do
    create table(:tags) do
      add :name, :string
      add :blog_id, references(:blogs, type: :uuid)

      timestamps
    end
    create index(:tags, [:blog_id])

  end
end
